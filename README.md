# readNames - simple node and express Application to read names form a text file and output it on screen.



## To run Application
Run following commands:

Initialize a new Node.js project:

    npm init -y
    
Install the express:

    npm install express
    
Run your Node.js script:

    node app.js
    
## Make sure to check the path of text file in app.js 

Certainly! Below is a simple README documentation for your GitLab CI/CD configuration:

---

# Node.js Express CI/CD Pipeline

This repository contains a simple GitLab CI/CD pipeline for a Node.js Express application. The pipeline has two stages: `build_stage` and `deploy_stage`.

## Build Stage

### Stage Description

The `build_stage` is responsible for setting up the build environment, installing dependencies, and creating artifacts.

### Job: Build

- **Image**: node
- **Script**:
  - `npm install`: Install project dependencies.

### Artifacts

The following artifacts are saved for later stages:

- `node_modules`: Node.js dependencies.
- `package-lock.json`: Package lock file.
- `names.txt`: Flat file to store names in the application.

## Deploy Stage

### Stage Description

The `deploy_stage` handles the deployment of the Node.js Express application.

### Job: Deploy

- **Image**: node
- **Script**:
  - `node app.js /dev/null 2>&1 &`: Start the application in the background.

## How to Use

1. **Build Stage**: Automatically triggered on every push to the repository.

2. **Deploy Stage**: Manually triggered or can be configured to run automatically after the build stage. This deploys the application.

**Note**: Ensure that the `names.txt` file is included in your repository with appropriate permissions for both reading and writing.

## Additional Information

- **Node.js Version**: The pipeline uses the `node` Docker image. Please ensure that the required Node.js version for your application is compatible with this image.

- **Customization**: Adjust the script and configurations according to your application's specific requirements.

- **Artifacts**: Artifacts generated during the build stage are saved and can be utilized for other purposes.

- **Continuous Integration/Continuous Deployment (CI/CD)**: This pipeline is a basic example. In a real-world scenario, you may need to enhance error handling, implement testing, and consider security measures.




